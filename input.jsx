

var Input = React.createClass({
  getInitialState: function(){
    // we don't want to validate the input until the user starts typing
    return {
      validationStarted: false
    };
  },


  prepareToValidate: function(){},
  componentWillMount: function(){
    var startValidation = function(){
      this.setState({
        validationStarted: true
      })
    }.bind(this);

    // if non-blank value: validate now
    if (this.props.value) {
      startValidation();
    }
    // wait until they start typing, and then stop
    else {
      this.prepareToValidate = _.debounce(startValidation, 1000);
    }
  },


  handleChange: function(e){
    if (!this.state.validationStarted) {
      this.prepareToValidate();
    }
    this.props.onChange && this.props.onChange(e);
  },


  render: function(){
    var className = "";
    if (this.state.validationStarted) {
       className = (this.props.valid ? "valid" : "invalid");
    }

    // return this.transferPropsTo(<input className={className} onChange={this.handleChange} />);
    // return (<input {...this.props} className={className} onChange={this.handleChange} />);

    this.props.className = className;
    return (<input {...this.props} onChange={this.handleChange} />);
  }
});


var App = React.createClass({
  getInitialState: function(){
    return {value: "", price: ""};
  },
  handleChange: function(e){
    this.setState({
      value: e.target.value
    })
  },
  handlePriceChange: function(e){
    this.setState({
      price: e.target.value
    })
  },
  validate: function(state){
    return {
      value: state.value.indexOf('react') !== -1,
      price: /^\$\d+\.\d+$/.test(state.price)
    }
  },
  render: function(){
    var valid = this.validate(this.state);
    return (
      <div>
        <Input valid={valid.value}
               className='foobar'
               value={this.state.value}
               onChange={this.handleChange}
               placeholder="something with 'react'"/>
        <Input valid={valid.price}
              value={this.state.price}
              onChange={this.handlePriceChange}
              placeholder="$0.00" />
      </div>
    );
  }
});

ReactDOM.render(
  <App />,
  document.getElementById('example')
);
